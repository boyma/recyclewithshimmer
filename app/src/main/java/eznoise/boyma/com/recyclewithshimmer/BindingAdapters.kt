package eznoise.boyma.com.recyclewithshimmer

import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.DecelerateInterpolator
import androidx.databinding.BindingAdapter

@BindingAdapter("animateVisibility")
fun animateVisibility(view: View, show: Boolean) {
    if (!show){
        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator() //add this
        fadeIn.duration = 1000
        view.animation = fadeIn
        view.startAnimation(fadeIn)
        fadeIn.fillAfter = true
    }
}