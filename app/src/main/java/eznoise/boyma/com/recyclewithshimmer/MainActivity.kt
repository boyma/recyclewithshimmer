package eznoise.boyma.com.recyclewithshimmer

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.github.nitrico.lastadapter.LastAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val list1 = listOf(
            CustItemType1("qwe0",false),
            CustItemType1("asd0",false),
            CustItemType1("zxc0",false),
            CustItemType1("qwe0",false),
            CustItemType1("asd0",false),
            CustItemType1("zxc0",false)
        )


        val genList = mutableListOf<Any>()
        repeat(6) {
            genList.add(CustItemType1(showShimmer = true))
        }

        LastAdapter(genList, BR.item)
            .map<CustItemType1>(R.layout.item_type1)
            .into(recycle)


        Handler().postDelayed({
            recycle.adapter = null
            LastAdapter(list1, BR.item)
                .map<CustItemType1>(R.layout.item_type1)
                .into(recycle)
            recycle.adapter?.notifyDataSetChanged()
        }, 4000)
    }
}
